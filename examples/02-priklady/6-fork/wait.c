#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>

int main(int argc, char *argv[])
{
    int ret = 0;
    pid_t f;
    if ((f = fork()) == 0) {
        ret = 10;
        printf("I'm child. My return is %i\n", ret);
        // same with exit(ret);
    } else {
        int child_ret;
        printf("Parent - variable ret has value %i\n", ret);
        pid_t w = waitpid(f, &child_ret, 0);
        printf("Child exit value %i wait returns %i fork returns %i\n", WEXITSTATUS(child_ret), w, f);
        printf("Parent / variable ret has value %i\n", ret);
    }
    return ret;
}
