#include <stdio.h>
#include <unistd.h>
#include <pthread.h>

volatile int a;
volatile int turn;

void *fce(void *n)
{
    int i, tmp;
    for (i = 0; i < 1000000; i++) {
        tmp = 1;
        asm volatile("xchg%z0  %2, %0;" : "=g"(turn), "=r"(tmp) : "1"(tmp) :);
        while (tmp != 0) {
            asm volatile("xchg%z0  %2, %0;" : "=g"(turn), "=r"(tmp) : "1"(tmp) :);
        }
        a += 1;
        turn = 0;
    }
    printf("a=%i\n", a);
    pthread_exit(NULL);
}

int main(int argc, char *argv[])
{
    pthread_t tid1, tid2;
    a = turn = 0;
    pthread_create(&tid1, NULL, fce, NULL);
    pthread_create(&tid2, NULL, fce, NULL);

    pthread_join(tid1, NULL);
    pthread_join(tid2, NULL);
    if (a == 2000000) {
        printf("OK\n");
    } else {
        printf("Fail %i\n", a);
    }
    return (a == 2000000) ? 0 : 1;
}
