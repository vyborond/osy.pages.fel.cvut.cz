#include <stdio.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <unistd.h>
#include <fcntl.h>
#include <err.h>

int main()
{
    char *shared_mem;
    int fd = open("/tmp/mapedfile", O_RDWR, 0600);
    if (fd >= 0) {
        printf("Open file %i\n", fd);
        shared_mem = mmap(NULL, 10000, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
        printf("Sdilena pamet %p\n", shared_mem);
        for (int i = 0; i < 1000; i++) {
            printf("Ulozeno: %s", shared_mem);
            shared_mem += 10;
        }
        close(fd);
    } else {
        err(1, "open");
    }
    return 0;
}
