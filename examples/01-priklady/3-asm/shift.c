#include <stdio.h>

int main()
{
   int a, b;
   unsigned int c, d;

   a = -16;
   asm volatile (
      "shr  $3, %%eax; "
      : "+a" (a) : : );
   printf("-16/8?=%i\n", a);

   a = -16;
   asm volatile (
      "sar  $3, %%eax; "
      : "+a" (a) : : );
   printf("-16/8?=%i\n", a);
   
   return 0;
}
