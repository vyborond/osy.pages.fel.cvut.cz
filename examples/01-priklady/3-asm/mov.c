#include <stdio.h>

int main()
{
   int a;
   unsigned int b;
   
   asm volatile (
      "mov  $0xffff, %%eax; "
      : "+a" (a) : :  );
   printf("a=%i\n", a);

   asm volatile (
      "movl $0xffff, %%ecx;  "
      "mov  %%ecx, %%eax; "
      : "+a" (a) : : "cx" );
   printf("a=%i\n", a);

   asm volatile (
      "movw $0xffff, %%cx;  "
      "movswl  %%cx, %%eax; "
      : "+a" (a) : : "cx" );
   printf("a=%i\n", a);
   
   asm volatile (
      "mov  $0xffffffff, %%eax; "
      : "+a" (a) : );
   printf("a=%i\n", a);

   asm volatile (
      "mov  $0xffff, %%eax; "
      : "+a" (b) : : );
   printf("b=%u\n", b);

   asm volatile (
      "movl $0xffff, %%ecx;  "
      "mov  %%ecx, %%eax; "
      : "+a" (b) : : "cx" );
   printf("b=%u\n", b);

   asm volatile (
      "movw $0xffff, %%cx;  "
      "movzwl  %%cx, %%eax; "
      : "+a" (b) : : "cx" );
   printf("b=%u\n", b);
   
   asm volatile (
      "mov  $0xffffffff, %%eax; "
      : "+a" (b) : );
   
   printf("b=%u\n", b);
   
   
   
   return 0;
}
