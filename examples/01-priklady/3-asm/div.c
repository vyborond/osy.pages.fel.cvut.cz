#include <stdio.h>

int main()
{
   int c_a, c_b;

   c_a = -15;
   c_b = -5;
   asm volatile (
      "mov   %%eax, %%edx;"
      "sar   $0x1f, %%edx;"
      "idivl  %%ebx; "
      : "+a" (c_a) : "b" (c_b) : "edx");
   printf("-15/(-5)?=%i\n", c_a);
   
   c_a = -15;
   c_b = -5;
   asm volatile (
      "xor   %%edx, %%edx;"
      "divl  %%ebx; "
      : "+a" (c_a) : "b" (c_b) : "edx" );
   printf("-15/(-5)?=%i\n", c_a);
   
   c_a = -15;
   c_b = -5;
   asm volatile (
      "mov   %%eax, %%edx;"
      "sar   $0x1f, %%edx;"
      "divl  %%ebx; "
      : "+a" (c_a) : "b" (c_b) : "edx" );
   printf("-15/(-5)?=%i\n", c_a);

   
   return 0;
}
