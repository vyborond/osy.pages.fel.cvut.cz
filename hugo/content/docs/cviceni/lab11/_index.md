---
title: "11. NOVA Malloc"
weight: 2
# bookFlatSection: false
# bookToc: true
# bookHidden: false
# bookCollapseSection: false
# bookComments: true
---

# Uživatelský paměťový alokátor pro OS NOVA
Na tomto cvičení si zkusíte implementovat alokátor dynamické paměti, který bude
využívat systémové volání `nbrk` z [minulého cvičení][lab10].

## Domácí příprava
Pro toto cvičení budete potřebovat:
- hotovou minulou úlohu
- zkontrolovat si, jestli [zdrojové kódy NOVA nebyly od minulé úlohy
  změněny][nova_log] a případně si [stáhnout aktuální verzi][nova_src]
- vědět, co je alokátor dynamické paměti (memory allocator) a způsoby
  implementace, viz:
  - [7. přednášku][l7]
  - http://arjunsreedharan.org/post/148675821737/write-a-simple-memory-allocator
- implementovat [spojové seznamy][1]
- rozumět programování v C/C++
- vědět, co dělají funkce [malloc a free][2]

[l7]: /docs/prednasky/pdf/lekce07.pdf
[1]: https://en.wikipedia.org/wiki/Linked_list
[2]: http://man7.org/linux/man-pages/man3/malloc.3.html
[lab10]: /docs/cviceni/lab10

[nova_log]: https://gitlab.fel.cvut.cz/osy/osy.pages.fel.cvut.cz/-/commits/master/nova
[nova_src]: https://gitlab.fel.cvut.cz/osy/osy.pages.fel.cvut.cz/-/archive/master/nova.zip?path=nova


# Zadání úlohy
Implementujte jednoduchý alokátor paměti pro uživatelský prostor OS NOVA. Váš
alokátor bude implementovat funkce `my_malloc` a `my_free` s následujícími
prototypy:
```C
void *my_malloc(unsigned long size);
int my_free(void *address);
```

Funkce implementujte v souboru [`user/mem_alloc.c`][mem_alloc.c] (C)
nebo `user/mem_alloc.cc` (C++) a na začátku souboru mějte *#include
\"[mem_alloc.h][]\"*. Makefile v adresáři user je na tento soubor
připraven a zkompiluje váš alokátor do knihovny `libc.a`. Tato
knihovna se linkuje k programům jako [`malloc_test.c`][malloc_test.c],
které můžete použít pro testování vašeho alokátoru.

[mem_alloc.h]: https://gitlab.fel.cvut.cz/osy/osy.pages.fel.cvut.cz/-/blob/master/nova/user/lib/mem_alloc.h
[mem_alloc.c]: https://gitlab.fel.cvut.cz/osy/osy.pages.fel.cvut.cz/-/blob/master/nova/user/mem_alloc.c
[malloc_test.c]: https://gitlab.fel.cvut.cz/osy/osy.pages.fel.cvut.cz/-/blob/master/nova/user/malloc_test.c

Od alokátoru budou očekávány následující vlastnosti:
- Bude schopen alokovat a uvolnit paměť.
- Při kompilaci nejsou generována žádná varování.
- Alokátor bude možné používat po přilinkování k aplikaci
  [`user/malloc_test.c`][malloc_test.c] ze zdrojových kódů OS NOVA.
- `my_malloc` alokuje paměť velikosti `size` a vrací adresu začátku alokované
  paměti. Pokud paměť požadované velikosti nelze alokovat, `my_malloc` vrací
  `0`.
- `my_free` uvolní paměť alokovanou funkcí `my_malloc`. Parametr address je
  hodnota dříve vrácená funkcí `my_malloc`. Pokud je paměť úspěšně uvolněna,
  funkce vrátí `0`, v opačném případě je vrácen nenulový kód chyby, který si
  můžete nadefinovat jak chcete.
- Pokud je `my_free` zavolána na již uvolněnou paměť nebo na paměť,
  která nebyla alokována voláním `my_malloc`, jedná se o chybu a
  funkce by ji měla signalizovat návratovou hodnotou. Můžete
  předpokládat, že program používající váš alokátor nemodifikuje jinou
  paměť na heapu než tu, vrácenou funkcí `my_malloc`.
- Bude používat systémové volání `nbrk` pro získání paměti pro alokaci.
- Paměťová režie alokátoru pro 16bytové alokace bude maximálně 100%. Tedy pokud
  např. zavolám 1024krát `my_malloc(16)`, alokátor si od jádra vyžádá voláním
  `nbrk` maximálně 2×1024×16 = 32 KiB paměti.
- Paměť uvolněná voláním `my_free` půjde znovu alokovat.
- Paměť alokovaná funkcí `my_malloc` bude přístupná minimálně do doby zavolání
  odpovídajícího `my_free`.
- Žádná část paměti alokované funkcí `my_malloc` nebude znovu alokována dříve,
  než bude zavoláno odpovídající `my_free`.

Nepovinně (pro plný počet bodů) budou navíc vyžadovány následující vlastnosti:
- Po uvolnění menších sousedních bloků bude možné ve stejné oblasti alokovat
  jeden velký souvislý blok.

**Co se odevzdává**: Archiv obsahující vaši implementaci v souboru
`user/mem_alloc.c` nebo `user/mem_alloc.cc` a soubor
`kern/src/ec_syscall.cc` z minulého cvičení. Můžete ho vytvořit
následujícím příkazem spuštěným z kořenového adresáře NOVY:

```bash
make hw11
```

# Ladění

- Úlohu můžete ladit pomocí Qemu a GDB, jak je popsáno u [minulé
  úlohy][lab10debug].

- Můžete také vyvíjet a ladit alokátor pod Linuxem a až když bude
  fungovat tam, budete ho překládat a testovat s Novou. V ukázkovém
  [`mem_alloc.c`][mem_alloc.c] je totiž i kód, který implementuje
  volání `nbrk` pomocí Linuxových `brk` a `sbrk`. Svůj alokátor a
  testovací program můžete přeložit pro Linux například následujícím
  příkazem:

  ```sh
  gcc -g -Og -Wall -I./lib -o malloc_test.linux malloc_test.c mem_alloc.c
  ```

- Stejné programy, kterými testuje BRUTE vaší implementaci si můžete
  spustit i lokálně:

  1. Stáhněte si [objektové soubory testovacích programů][testprogs].
  2. Rozbalte je do adresáře `nova/user`:
     ```sh
     tar xf malloc_tests.tgz
     ```
  3. Slinkujte je se svou implementací (stačí použít `make` se jménem cílové binárky):
     ```shell-session
     $ make malloc-3m3f malloc-basic ...
     g++ -m32 -g -nostdlib -fomit-frame-pointer -fno-stack-protector -no-pie -Og -Wall -Ilib -DNOVA -fno-rtti -fno-exceptions -Wl,--build-id=none -Wl,--gc-sections -Wl,-Map=mallf
     g++ -m32 -g -nostdlib -fomit-frame-pointer -fno-stack-protector -no-pie -Og -Wall -Ilib -DNOVA -fno-rtti -fno-exceptions -Wl,--build-id=none -Wl,--gc-sections -Wl,-Map=mallc
     ...
     ```
  4. Spusťte výsledek:
     ```sh
     qemu-system-i386 -nographic -kernel ../kern/build/hypervisor -initrd  malloc-3m3f
     ```
     Můžete přidat i parametry `-s -S` a ladit kód v debuggeru.


[lab10debug]: /docs/cviceni/lab10#ladění
[testprogs]: misc/malloc_tests.tgz
